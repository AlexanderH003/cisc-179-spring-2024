Welcome to text adventure.
Would you like instructions?, '[y]es' or '[n]o'
yes
Ok, hold onto your horses
You are at the fork in the road. Where would you like to go? Go to the road on the "[l]eft" or the road on the "[r]ight".
You ran into an inevitable roadblock. Sorry. Adventure over.
